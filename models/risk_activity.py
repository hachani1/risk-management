from odoo import models, api, fields

class RiskActivity(models.Model):

     _inherit = "mail.activity"
     _name = "risk.activity"

     #------------------
     #-------fields-----
     #-----------------

     activity_degree = fields.Integer("activity degree")
