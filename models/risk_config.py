from odoo import models,fields,api,_

class RisksConfig(models.Model):

    _name="risk.config"
    _description="risk configuration"
    _inherit = ['risk.disaster','risk.action']


class RisksType(models.Model):
    _name="risk.type"
    _description="risk configuration"

    #----------------
    #------fields----
    #---------------

    risk_name = fields.Many2one("risk.disaster",string="risk name")
    risk_type = fields.Selection([('naturels', 'Naturels'), ('sanity', 'Sanity')
                                     , ('profesional', 'Profesional'), ('psychological', 'Psychological')
                                     , ('technologies', 'Technologies'),
                                  ('digital', 'Digital'), ('financial', 'Financial'),
                                  ('geographical', 'Geographical'), ('géopolitic', 'Géopolitic'),
                                  ('climatic', 'climatic')], string="risk type")





