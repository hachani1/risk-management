import self as self
from odoo import models,fields,api,_
import re
from odoo.exceptions import ValidationError

class RisksDisaterInformations(models.Model):

    _name = "risk.disaster"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "risk information"
    _rec_name ='risk_name'

    #----------------------------------------
    #filelds--------------------------------
    #---------------------------------------

    risk_id = fields.Many2one('risk.action',string='Risk ID')
    risk_name = fields.Char(string="Risk Name",required=True)
    status = fields.Selection([('draft','Draft'),('inprogress','Inprogress'),('closed','Closed')],string="Risk Status",default="draft",required=True)
    category = fields. Selection(
        [('naturels','Naturels'),('sanity','Sanity')
            ,('professionels','Professionels'),('psychological','Psychological')
            ,('technological','Technological'),
         ('digital','Digital'),('financial','Financial'),
         ('geographical','Geographical'),('géopolitic','Géopolitic'),
         ('climatic','climatic')]
        ,help="Risk Category: The type of risk in terms of the project's or business' chosen categories (e.g. Schedule, quality, legal etc.)",string="Risk Type",required=True)
    description = fields.Char(string='Risk Description', help="Short description of the Risk.",required=True)
    start_datetime = fields.Date(string="StartDateTime",help="Date of the Risk registered. Auto populated.")
    location_id = fields.Many2one("hr.work.location",string='Department Location',required=True)
    department_id = fields.Many2one("hr.department",string="Department")
    source = fields.Selection([('external','External'),('internal','Internal')],string="Risk Source",required=True)
    end_datetime = fields.Date(string="EndDateTime")
    priority_level = fields.Integer(string="Priority Level",default="0",required=True)
    users_id = fields.Many2one("res.users",string="Relared Users")
    risk_Owner = fields.Char(string="Owner")
    company_id = fields.Many2one("res.partner",string="Company Name",required=True)
    partner_id = fields.Many2one("res.partner")
    witness_name = fields.Char("Witness Name")
    witness_job_id = fields.Many2one("hr.job")
    witness_category = fields.Selection([('external','External'),('internal','Internal')])
    witness_phone = fields.Char("Mobile phone")
    witness_email = fields.Char(string="Email")
    witness_location = fields.Char("Submit your geographical Location ")
    witness_street = fields.Char("Street")
    witness_city = fields.Char("City")
    witness_country = fields.Char("Country")
    witness_street_number = fields.Integer("Street Number")
    image_1920 = fields.Image(readonly=False, store=True, copy=False)
    risk_latitude = fields.Float(string="Geo Latitude", digits=(10, 7))
    risk_longitude = fields.Float(string="Geo Longitude", digits=(10, 7))
    risk_street = fields.Char(string="Street Name")
    risk_city = fields.Char(string="City Name")
    country_state_id = fields.Many2one("res.county.state", string="State")
    country_id = fields.Many2one("res.country", string="Country")
    street_number = fields.Integer(string="Street number",default=0)
    count = fields.Integer(string="Somme",compute="create")
    cause_description = fields.Text("Cause Description",required=True)
    code = fields.Char("Code")
    risk_request=fields.Selection([('avoid','avoid'),('refuse','refuse')],string="Requested Risk")
    risk_count = fields.Integer(string="Risk Count",compute="count_risk")

    # ----------------------------------------
    # risks methods---------------------------
    # ---------------------------------------

    def set_state_draft(self):
        return self.write({'status': 'draft'})

    def set_state_inprogress(self):
        return self.write({'status': 'inprogress'})

    def set_state_closed(self):
        return self.write({'status': 'closed'})


    @api.constrains('start_datetime', 'end_datetime')
    def _check_date_time(self):
        for rec in self:
            if rec.start_datetime and rec.end_datetime and rec.end_datetime < rec.start_datetime:
                raise ValidationError(_('invalid field,End Date Must be greater Than Start Date...'))


    @api.constrains('witness_email')
    def _check_mail(self):
        for rec in self:
            if rec.witness_email and re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', rec.witness_email) == None:
                raise ValidationError('Not a valid E-mail')


    @api.constrains('priority_level')
    def _check_level_priority(self):
        """check priority level"""
        for rec in self:
            if  rec.priority_level and rec.priority_level > 5 :
                raise ValidationError(_('priority level is out of range,maximum level is 5'))

    @api.model
    def create(self, vals):
        risk = super(RisksDisaterInformations, self).create(vals)
        if risk:
            users = self.env.ref("risks_disaster_management.group_risk_cordinator").users
            for user in users:
                 risk.activity_schedule("risk_disaster.mail_activity_risk", user_id=user.id)
        return risk

    def count_risk(self):
        for rec in self:
            rec.risk_count = self.env['risk.disaster'].search_count([('risk_name', '!=' ,'' )])


















































