from odoo import models,fields,api,_

class RisksDisasterCauses(models.Model):

    _name = "risk.cause"
    _description = "risk causes"

    #----------------------------------------
    #filelds--------------------------------
    #---------------------------------------

    cause_id = fields.Many2one("risk.disaster",string="causes refers to risk",required=True)
    description = fields.Text(string="Cause Description",default="cause description")#relier au risk
    source = fields.Selection([('external','External'),('internal','Internal')],string="Cause source")
    category = fields.Selection([('naturels','Naturels'),('sanity','Sanity'),('profesional','Profesional'),('psychological','Psychological'),('technologies','Technnologies'),('digital','Digital'),('financial','Financial'),('geographical','Geographical'),('geopolitic','Geopolitic'),('climatic','climatic')])
    responsible_ids = fields.Many2many("hr.employee",string="responsible(s)")
    responsible_category = fields.Selection([('external','External'),('internal','Internal')],string="Responsible  Category")
    cause_name = fields.Char("Cause Name")
    location_id = fields.Many2one("hr.work.location",string="cause location")
    image_1920 = fields.Image(readonly=False, store=True)












