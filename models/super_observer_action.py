import re
from odoo import models,fields,api,_
from odoo15.odoo.exceptions import ValidationError

class SuperObserverAction(models.Model):
    _name = "observer.action"
    _description = "super observer  Actions"

    #------------------------------------
    #fields------------------------------
    #------------------------------------

    presence_status = fields.Selection([('Left','left'),('Present','present')],string="Presence status")
    available = fields.Selection([('Available','available'),('Unvailable','unvailable')])
    image_1920 = fields.Image(readonly=False, store=True)
    street = fields.Char("Street")
    city = fields.Char("City")
    street_number = fields.Integer("Street number")
    observer_name = fields.Char(string="Observer name")
    job_id = fields.Many2one("hr.job",string="Job")
    phone = fields.Char(string="Phone number")
    email = fields.Char(string="Email")
    action_id = fields.Many2one("risk.action",string="Select/Create action")
    description = fields.Text("risk description")
    risk_id = fields.Many2one("risk.disaster",string="Requested risk")
    status = fields.Selection([('Accept','accept'),('Transfer to','transfer to')])
    risk_information = fields.Text(string="Actual information")
    crisis_information = fields.Text("Send crisis information")
    category = fields.Selection([('External','external'),('Internal','internal')])
    apropriate_employee_id = fields.Many2one("hr.employee",string="Transfer to")
    crisis_status = fields.Selection([('Draft','draft'),('Inprogress','inprogress'),('Closed','closed')],string="risk status")
    mission_status = fields.Selection([('Failed','failed'),('Sucess','sucess')])
    mission_type = fields.Selection([('People', 'people'), ('Facilities', 'facilities')
                                           , ('Systems', 'systems'), ('Equipment', 'equipment')
                                            , ('Materials', 'materials'),
                                         ('Supplies', 'supplies'),
                                         ('Funding', 'funding'), ('Information', 'information'),
                                         ('Public Emergency Service', 'public emergency service'),
                                         ('Logistics', 'logistics'),
                                         ('Communications And Warning Technologies',
                                          'Communications and warning technologies'),
                                         ('Fire protection and life safety systems',
                                          'fire protection and life safety systems'),
                                         ('Pollution control systems', 'pollution control systems'),
                                         ('Information about the threats or hazards',
                                          'information about the threats or hazards'),
                                         ('Special expertise', 'special expertise')])
    external_res_id = fields.Many2one("res.partner",string="Transfer to external Ressource")

    def set_state_draft(self):
        if self.crisis_status == 'closed':
            self.crisis_status = 'draft'

    def set_state_inprogress(self):
        if self.crisis_status=='draft':
            self.crisis_status='inprogress'

    def set_state_closed(self):
        if self.crisis_status == "inprogress":
            self.crisis_status = "closed"

    @api.constrains('email')
    def validate_mail(self):
        for rec in self:
                if  rec.email and re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$',rec.email) == None:
                    raise ValidationError('Not a valid E-mail')

































