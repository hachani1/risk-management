from odoo import models,fields,api,_
from datetime import date, datetime
from odoo.exceptions import ValidationError

class RisksDisasterActions(models.Model):

    _name = "risk.action"
    _description = "risk Actions"

    #----------------------------------------
    #filelds--------------------------------
    #---------------------------------------

    user_id = fields.Many2one("res.users")
    action_id = fields.Many2one("risk.disaster",required=True)
    progress_action = fields.Float(string="Action progress" ,default="20" )
    source = fields.Selection([('external','External'),('internal','Internal')],string="Action Source ",required=True)
    success = fields.Float(string="Sucess Pourcentage",default="20")
    description = fields.Text(string="Description")
    status = fields.Selection([('draft','Draft'),('inprogress','Inprogress'),('closed','Closed')],string='Action Status',required=True)
    risk_owner_ids = fields.Many2many("hr.employee",string="Owner(s)")
    name = fields.Char(string="Action Name",required=True)
    category = fields.Selection([('naturels','Naturels'),('sanity','Sanity')
            ,('profesionel','Profesional'),('psycological','Psycological')
            ,('technologies','Technologies'),
         ('digital','Digital'),('financial','Financial'),
         ('geographical','Geographical'),('geopolitic','Geopolitic'),
         ('climatic','Climatic')],required=True)
    start_date = fields.Date(string="Start Datetime")
    end_date = fields.Date(string=" End Datetime")
    department_id = fields.Many2one("hr.department",required=True)
    ext_mission = fields.Char("Select mission to external Ressource")
    int_mission = fields.Char("Select mission to internal Ressource")
    location_id = fields.Many2one("hr.work.location",requires=True)
    emergency_degree=fields.Selection([('need for resuscitation','need for resuscitation'),('emergency','emergency'),('urgent','urgent'),('semi-urgent','semi-urgent'),('non-urgent','non-urgent')],string="Emergency level")
    ext_res_category = fields.Selection([('People', 'people'), ('Facilities', 'facilities')
                                            , ('Systems', 'systems'), ('Equipment', 'equipment')
                                            , ('Materials', 'materials'),
                                         ('Supplies', 'supplies'),
                                         ('Funding', 'funding'), ('Information', 'information'),
                                         ('Public Emergency Service', 'public emergency service'),
                                         ('Logistics', 'logistics'),
                                         ('Communications And Warning Technologies',
                                          'Communications and warning technologies'),
                                         ('Fire protection and life safety systems',
                                          'fire protection and life safety systems'),
                                         ('Pollution control systems', 'pollution control systems'),
                                         ('Information about the threats or hazards',
                                          'information about the threats or hazards'),
                                         ('Special expertise', 'special expertise')],
                                        string=" Category")
    risk_status = fields.Selection([('draft','Draft'),('inprogress','Inprogress'),('closed','Closed')],string="Risk Status")
    availibilty=fields.Boolean("Availibilty")
    result = fields.Selection([('sucess','sucess'),('failed','failed')],string="Action Result")

    #---------------------------------------------------
    #methods--------------------------------------------
    #---------------------------------------------------

    def set_state_risk_draft(self):

        return self.write({'risk_status': 'draft'})

    def set_state_risk_inprogress(self):
        return self.write({'risk_status': 'inprogress'})

    def set_state_risk_closed(self):
        return self.write({'risk_status': 'closed'})

    def maps_url(self):
        if self.id:
            return {
                'type': 'ir.actions.act_url',
                'url': 'https://www.google.com/maps/@36.8712709,10.305536,14z=%s' % (self.id),
                'target': 'new',
            }


    def button_url_cantact(self):
        if self.id:
            return {
                'type': 'ir.actions.act_url',
                'url': 'http://0.0.0.0:8015/web#cids=1&menu_id=111&action=139&model=res.partner&view_type=list=%s' % (self.id),
                'target': 'new',
            }

    @api.constrains("start_date","end_date")
    def _check_date(self):
        for rec in self:
            if rec.end_date and rec.start_date and rec.start_date > rec.end_date:
                raise ValidationError(_('invalid field,End Date Must be greater Than Start Date...'))















