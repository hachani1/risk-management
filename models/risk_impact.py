from odoo import models,fields,api,_
from datetime import date
from odoo.exceptions import ValidationError

class RisksDisaterImpacts(models.Model):

    _name = "risk.impact"
    _description = "risk information"

    # ----------------------------------------
    # fields--------------------------------
    # ---------------------------------------

    victim_id = fields.Many2one("hr.employee",string="Victim Employee")
    risk_id = fields.Many2one("risk.disaster",string="Requested Risk")
    description = fields.Text(string='Impact Description')
    status = fields.Selection([('draft','Draft'),('closed','Closed')],string='Impact Status')
    category = fields.Selection([('naturels','Naturels'),('sanitaire','Santaire')
            ,('professionnels','Professionnels'),('psychosociaux','Psychosociaux')
            ,('technologiques','Technologiques'),
         ('numériques','Numériques'),('financier','Financier'),
         ('geographiques','Geographiques'),('géopolitique','Géopolitiques'),
         ('cimatique','climatique')],string="Impact Category")
    impact_degree = fields.Selection([('Severe','severe'),('Significant','significant'),('Moderate','moderate'),('Minor','minor'),('Minimal','minimal')],string='Impact Degree')
    heath_safety = fields.Selection([('catostrophic','likely daths and many with seriuos injuries'),
    ('very critical','possibly deaths and many with serious injuries and/or grave diseases')
    ,('critical','some with serius injuries and/or grave diseases'),
    ('dangerous','few with minor injuries and/or diseases'),
    ('not dangerous','minimal or no harm to person')],string="Health and safety human being")

    monetory_values = fields.Selection([('catostrophic','Economic loss >60 million EUR'),
    ('very critical','Economic loss 12_60 million EUR'),('critical','Economic loss 1_12 million EUR'),
    ('dangerous','Economic loss 10000_1 million EUR'),('not dangerous','Economic loss<10000 EUR')],string="Monetory values/Economic issue")
    environment_impact = fields.Selection([
    ('catostrophic','Large uncontrolled release/Regional impact with repartive effort>10 years'),
    ('very critical','Large release/Local impact with repartive effort 3_10 years'),
    ('critical','Significant release/Repartive effort 1_3 years'),
    ('dangerous','Small release/Detectable damage with repartive effort>1 year'),
    ('not dangerous','Minor release/No detectable damage')],string="Environmental Impact")
    organization_impact = fields.Selection([
    ('catostrophic','Internation media attention./Public inquiry/Organization must close down'),
    ('very critical','Internation media attention/Public inquiry/High impact on organization'),
    ('critical','National media attention/Public inquiry/Moderate impact on organization'),
    ('dangerous','Possible local media attention/Possible public inquiry/Low impact on organization'),
    ('not dangerous','Just organizational attention/Just limited impact on organization')],string="Organization Reputation")
    probability = fields.Selection([('not dangerous','rare'),('dangerous','unlikely'),('critical','likely'),('very critical','very likely')],string="Probability")
    impact_level = fields.Integer(string="Risk likelihood",required=True)
    rating = fields.Integer(string="Risk Rating",required=True)
    score = fields.Integer(string="Risk Score",compute="calcul_risk_score")
    score_state = fields.Char(string="Risk State",compute="score_risk_state")

    #-----------------------
    #-------methods---------
    #----------------------

    @api.constrains('impact_level')
    def _check_risk_like_hold(self):

        for rec in self:
            if rec.impact_level and rec.impact_level > 5:
                raise ValidationError(_('risk likehold is out of range,maximum level is 5'))


    @api.constrains('rating')
    def _check_risk_rating(self):

        for rec in self:
            if  rec.rating and rec.rating > 5:
                raise ValidationError(_('risk rating is out of range,maximum level is 5'))


    @api.depends('impact_level','rating')
    def calcul_risk_score(self):
        for rec in self:
            rec.score = rec.rating * rec.impact_level
        return rec.score


    @api.depends('score')
    def score_risk_state(self):
        for rec in self:
            if rec.score in range(6):
                rec.score_state = "Low"
            elif rec.score in range(5,17):
                rec.score_state = "Medium"
            else:
                rec.score_state ="High"
        return rec.score_state






