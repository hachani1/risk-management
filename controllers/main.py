from odoo import http, models, fields, _
from odoo.http import request
from odoo.addons.website_customer.controllers.main import WebsiteCustomer

class Redirect(http.Controller):


    @http.route('/risk_disaster',type="http",auth="public",website=True)
    def risk_disaster(self,**kwargs):

        locations = request.env['hr.work.location'].sudo().search([])
        companies= request.env['res.partner'].sudo().search([])
        return request.render('risks_disaster_management.create_risk',{'locations':locations,
                                                                       'companies':companies
                                                                       })

    @http.route('/create/risk', type="http", auth="public", website=True)
    def create_risk(self, **kw):
        request.env['risk.disaster'].sudo().create(kw)
        return request.render('risks_disaster_management.create_risk', {

        })





